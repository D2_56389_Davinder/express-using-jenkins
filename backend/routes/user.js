const express = require('express');
const utils = require('../utils');
const db = require('../db')

const router=express.Router()

router.get("/",(request,response)=>
{
    const connection =db.openconection()
    const statement=`select id,name,age,email from user`
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createresult(error,result))
    })
})


router.post("/",(request,response)=>
{
    const {name,age,email} = request.body
    const connection =db.openconection()
    const statement=`insert into user (name,age,email) values 
                    ('${name}','${age}','${email}')`
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createresult(error,result))
    })
})


router.put("/",(request,response)=>
{
    const {id,name,age,email} = request.body
    const connection =db.openconection()
    const statement=`update user set name = '${name}',age='${age}',email='${email}' where id=${id}`
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createresult(error,result))
    })
})


router.delete("/",(request,response)=>
{
    const id = request.body.id
    const connection =db.openconection()
    const statement=`delete from user where id = ${id}`
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createresult(error,result))
    })
})

module.exports=router