const express = require('express');
const cors = require('cors');
const userRouter = require('./routes/user');
const app = express()

app.use(express.json())
app.use("/user",userRouter)
app.use(cors("*"))
app.use("/",(request,response)=>
{
    response.send(`welcome to our new express app.....`)
})

app.listen(4000,"0.0.0.0",()=>
{
    console.log(`server started on port 4000`)
})