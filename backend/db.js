const mysql = require('mysql2');

const openconection=()=>
{
    const connection = mysql.createConnection(
        {
            host : "demodb",
            user : "root",
            password : "root",
            database : "mydb",
        })

    connection.connect()
    
    return connection

}

module.exports={
    openconection,
}